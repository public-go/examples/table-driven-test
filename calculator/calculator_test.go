package calculator

import (
	"testing"
)

func TestAdd(t *testing.T) {
	var tests = []struct {
		name string
		inX  int
		inY  int
		out  int
	}{
		{"simple addition", 5, 5, 10},
		{"simple more complex example", 5, -10, -5},
	}

	for _, tc := range tests {
		got := Add(tc.inX, tc.inY)
		if got != tc.out {
			t.Errorf("%q # Add(%d, %d) == %d, want %d", tc.name, tc.inX, tc.inY, got, tc.out)
		}
	}
}
