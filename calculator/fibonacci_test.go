package calculator

import (
	"strconv"
	"testing"
)

func TestFibonaci(t *testing.T) {
	var tests = []struct {
		in  int
		out []int
	}{
		{1, []int{1}},
		{7, []int{1, 1, 2, 3, 5, 8, 13}},
		{6, []int{1, 1, 2, 3, 5, 8}},
	}

	for _, tc := range tests {
		got := Fibonaci(tc.in)
		if slicesAreNotEqual(got, tc.out) {
			t.Errorf("With n: %d, got: %v, want %v", tc.in, intSlice2String(got), intSlice2String(tc.out))
		}
	}
}

func intSlice2String(a []int) (output string) {
	output = "["
	for i, num := range a {
		if i != 0 {
			output += ", "
		}
		output += strconv.Itoa(num)
	}
	output += "]"
	return
}

func slicesAreNotEqual(a, b []int) bool {
	if len(a) != len(b) {
		return true
	}
	for i := range a {
		if a[i] != b[i] {
			return true
		}
	}
	return false
}
