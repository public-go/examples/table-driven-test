// Package calculator contains a simple calculator
package calculator

// Add adds x and y
func Add(x int, y int) int {
	return x + y
}

// Fibonaci creates n fibonaci numbers and returns a slice
func Fibonaci(n int) (allNumbers []int) {
	a := 1
	b := 0

	for i := 1; i <= n; i++ {
		// fmt.Print(a, ",")
		allNumbers = append(allNumbers, a)
		a, b = nextIter(a, b)
	}
	return
}

// nextIter prepares the next iteration
func nextIter(a int, b int) (c int, d int) {
	c = a + b
	d = a

	return
}
