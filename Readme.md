# perform table-driven-tests

To be able to run these examples first checkout this repo:
```bash
go get -v gitlab.com/public-go/examples/table-driven-test
```

Go to the directory calculator and run tests:
```bash
cd $GOPATH/src/gitlab.com/public-go/examples/table-driven-test/calculator
go test -v 
```

IF you also want test coverage:
```bash
go test -cover
```